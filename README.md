# Transactions Seeker Backend

### Mandatory:

- Install Node.js v10.15.2 and npm v6.9.0

  > To easily install and manage versions of node and npm is recommended to use [nvm](https://github.com/creationix/nvm).

  1. Install nvm:  
     `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash`
  2. Reload the shell:  
     `source ~/.bashrc`
  3. Verify that nvm has been installed:  
     `command -v nvm`
  4. List Node.js versions installed:  
     `nvm ls`
  5. Install Node.js v10.15.2 and npm v6.9.0:  
     `nvm install v10.15.2`
  6. Check the Node.js versions installed again:  
     `nvm ls`

- After install node and npm, in the project root run this command:

  `npm install`

## Documentation

- To create the documentation use [typedoc](https://typedoc.org/).

- To generate the documentation execute this commands in the project root:

  `npm run docs:generate`

- Then you will found the generated documentation in `docs/` directory

## Development environment configuration:

- Export this environment variables:

  * **TS_MONGODB_URI**
  * **TS_API_PORT**
  
## Unit tests

- to execute the tests run in the terminal `npm run test` || `npm run test:coverage`

- You will find the tests coverage report in the ./coverage/lcov-report/index.html file.

## SonarQube Scan

- Export this environment variable

  * **TS_SONAR_URL**

## Build

- Run npm install

- Export this environment variables:

  * **TS_MONGODB_URI**
  * **TS_API_PORT**

- And run `npm run build:prod`

## Deployment

- Export this environment variables:  
  
  * **TS_API_PORT**
  * **TS_DB_PORT**
  * **DB_VOLUME_PATH**
  * **LOGS_VOLUME_PATH**
  
- Build the application

- And run:
  `docker-compose --project-name=transactions-seeker-backend build`
  `docker-compose --project-name=transactions-seeker-backend down`
  `docker-compose --project-name=transactions-seeker-backend up -d`

- For monitoring and logging, this project use [pm2](http://pm2.keymetrics.io/).
