FROM node:10.15.2

ARG TS_DB_PORT

ENV TS_MONGODB_URI mongodb://mongo:27017/transactions-seeker

WORKDIR /usr/src/app

RUN mkdir logs

COPY ./package.json ./

COPY ./node_modules ./node_modules

RUN npm i --production

RUN npm install pm2 -g

COPY ./dist ./dist

COPY ./pm2.config.js ./

CMD [ "pm2-runtime", "start", "pm2.config.js", "--only", "transactions-seeker" ]
