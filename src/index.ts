import { ApolloServer } from 'apollo-server';
import mongoose from 'mongoose';

import { environment } from './environments';
import { schema } from './graphql';

mongoose.connect(
  environment.tsMongodbUri,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
  },
);

const apolloServer = new ApolloServer({
  schema,
  formatError(error) {
    if (!environment.production) {
      console.log(error);
    }
    return error;
  },
});

export const serverConnection = apolloServer.listen(
  {
    port: environment.tsPort,
  },
).then(({ server, url, subscriptionsUrl }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
  return { server, url, subscriptionsUrl };
});

if (module.hot) {
  module.hot.accept();
  module.hot.dispose(
    () => console.log('Module disposed.'),
  );
}
