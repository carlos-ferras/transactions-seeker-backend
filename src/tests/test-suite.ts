import 'cross-fetch/polyfill';
import { expect } from 'chai';
import mongoose from 'mongoose';
import { graphql } from 'graphql';
import { Mockgoose } from 'mockgoose';
import ApolloClient from 'apollo-boost';

import { schema } from '../graphql';

const internalDescribeQL = (describeFn) => (title, body) => {
  describeFn(title, () => {
    let apollo;
    let expressServer;
    const mockgoose = new Mockgoose(mongoose);

    before(async () => {
      await mockgoose.prepareStorage();
      await require('../index').serverConnection.then(({server, url}) => {
        expressServer = server;

        apollo = new ApolloClient({
          uri: url
        });
      });
    });

    after(done => {
      expressServer.close();
      mongoose.disconnect(done);
    });

    beforeEach(() => {
      mockgoose.helper.reset();
    });

    const expectQuery = async (query, expected) => {
      expect(
        await graphql(
          schema,
          query,
          undefined,
          {},
        ),
      ).to.deep.equal(expected);
    };

    const expectQueryResult = (query, expected) => {
      return expectQuery(
        query,
        {
          data: expected,
        },
      );
    };

    const execGraphQL = (query) => graphql(
      schema,
      query,
      undefined,
      {},
    );

    const client = () => apollo;

    body({
      expectQuery,
      expectQueryResult,
      execGraphQL,
      client,
    }, schema);
  });
};

export const describeGQL: any = internalDescribeQL(describe);
describeGQL.only = internalDescribeQL(describe.only);
describeGQL.skip = internalDescribeQL(describe.skip);
