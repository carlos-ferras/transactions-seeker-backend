import { expect } from 'chai';
import { gql } from 'apollo-boost';

import { describeGQL } from './test-suite';

describeGQL('Test name', ({ execGraphQL, client }) => {
  it('test case', async () => {
    await execGraphQL(
      `mutation AddTransaction {
        addTransaction(input: {
          id: "test",
          name: "Test name",
          email: "test@email",
          phone: "55-555",
          age: 55,
          geoInfo: {
            latitude: 5,
            longitude: 5
          },
          childrens: [
            {
              id: "test-child",
              name: "Test name",
              email: "test.child@email",
              phone: "44 444",
              age: 44,
              geoInfo: {
                latitude: 4,
                longitude: 4
              },
              connectionInfo: {
                type: "sameName",
                confidence: 0.7
              }
            }
          ]
        }) {
          id,
          name,
          email,
          phone,
          age,
          geoInfo {latitude, longitude}
        }
      }`,
    );

    const subscriptionPromise = new Promise((resolve, reject) => {
      client().subscribe({
        query: gql`
          fragment transactionFields on Transaction {
            id,
            name,
            email,
            phone,
            age,
            geoInfo {
              latitude, 
              longitude
            },
          }
          
          fragment transactionChildFields on  Transaction {
           ...transactionFields,
            connectionInfo {
              type,
              confidence
            }
          }
          
          fragment transactionRecursive on  Transaction {  
            ...transactionFields,
            childrens {
              ...transactionChildFields,
              childrens {
                ...transactionChildFields
              }
            }
          }

          query {
            transactions {
              ...transactionRecursive
            }
          }`,
      }).subscribe({
        next: resolve,
        error: reject,
      });
    });

    expect((await subscriptionPromise as any).data).to.deep.equal({
      transactions: [
        {
          __typename: 'Transaction',
          id: 'test',
          name: 'Test name',
          email: 'test@email',
          phone: '55-555',
          age: 55,
          geoInfo: {
            __typename: 'GeoInfo',
            latitude: 5,
            longitude: 5,
          },
          childrens: [
            {
              __typename: 'Transaction',
              id: 'test-child',
              name: 'Test name',
              email: 'test.child@email',
              phone: '44 444',
              age: 44,
              geoInfo: {
                __typename: 'GeoInfo',
                latitude: 4,
                longitude: 4,
              },
              connectionInfo: {
                __typename: 'ConnectionInfo',
                type: 'sameName',
                confidence: 0.7,
              },
              childrens: [],
            },
          ],
        },
      ],
    });
  });
});
