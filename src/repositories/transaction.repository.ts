import Transaction from '../models/transaction.model';

export const TransactionRepository = {
  getAll: async () => {
    const transactions: any[] = await Transaction.find({}, null);
    return transactions.map(transaction => transaction.toGraph());
  },

  getById: async (id) => {
    const transaction: any = await Transaction.findById(id);
    return transaction.toGraph();
  },

  create: async (data) => {
    const transaction: any = await Transaction.create(data);
    return transaction.toGraph();
  },

  update: async (id, data) => {
    const transaction: any = await Transaction.findByIdAndUpdate(id, data);
    return transaction.toGraph();
  },

  delete: async (id) => {
    const transaction: any = await Transaction.findByIdAndRemove(id);
    return transaction ? transaction.toGraph() : null;
  },
};
