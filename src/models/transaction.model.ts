import mongoose from 'mongoose';

import {transactionSchema} from "./schemas";

transactionSchema.set(
    'toObject',
    {
        virtuals: true
    }
);

transactionSchema.method(
    'toGraph',
    function toGraph(this: any) {
        return JSON.parse(
            JSON.stringify(this)
        );
    }
);

export default mongoose.model('Transaction', transactionSchema);
