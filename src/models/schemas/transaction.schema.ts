import mongoose from 'mongoose';
import Email from 'mongoose-type-email';

import {geoInfoSchema} from "./geo-info.schema";
import {connectionInfoSchema} from "./connection-info.schema";

const transactionSchema = new mongoose.Schema();
transactionSchema.add({
    id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: Email,
        required: true,
    },
    phone: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true,
        min: 0,
        max: 130
    },
    geoInfo: {
        type: geoInfoSchema,
        required: true
    },
    connectionInfo: {
        type: connectionInfoSchema
    },
    childrens: {
        type: [transactionSchema]
    }
});

export {
    transactionSchema
}
