import mongoose from 'mongoose';

export const connectionInfoSchema = new mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    confidence: {
        type: Number,
        min: 0,
        max: 1,
        required: true
    }
}, { _id: false });
