export const sharedEnvironment = {
    tsMongodbUri: process.env.TS_MONGODB_URI || '',
    tsPort: process.env.TS_API_PORT || 4500,
};
