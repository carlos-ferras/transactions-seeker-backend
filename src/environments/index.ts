const env = ['prod', 'test'].includes(process.env.TS_ENV || '')
    ? `${process.env.TS_ENV}.`
    : '';

export const environment = require(`./environment.${env}ts`).environment;
