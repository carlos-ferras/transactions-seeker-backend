export const environment = {
  production: true,
  testing: true,
  tsMongodbUri: 'mongodb://localhost/transactions-seeker-test'
};
