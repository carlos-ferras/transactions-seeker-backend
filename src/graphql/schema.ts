import { makeExecutableSchema } from 'graphql-tools';

import { rootTypeDefs, transactionTypeDefs } from './types';
import { transactionResolvers } from './resolvers';

export const schema = makeExecutableSchema({
  typeDefs: [rootTypeDefs, transactionTypeDefs],
  resolvers: transactionResolvers,
});
