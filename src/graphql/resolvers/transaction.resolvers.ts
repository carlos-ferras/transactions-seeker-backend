import { PubSub } from 'apollo-server';

import { TransactionRepository } from '../../repositories';

const TRANSACTION_CREATED = 'TRANSACTION_CREATED';
const TRANSACTION_UPDATED = 'TRANSACTION_UPDATED';
const TRANSACTION_DELETED = 'TRANSACTION_DELETED';
const pubsub = new PubSub();

export const transactionResolvers = {
  Query: {
    transactions: async (_, {}) => {
      return TransactionRepository.getAll();
    },
    transaction: async (_, { id }) => {
      return TransactionRepository.getById(id);
    },
  },

  Mutation: {
    addTransaction: async (_, { input }) => {
      const transaction: any = TransactionRepository.create(input);
      await pubsub.publish(
        TRANSACTION_CREATED,
        {
          transactionCreated: transaction,
        },
      );
      return transaction;
    },
    updateTransaction: async (_, { id, input }) => {
      const transaction: any = await TransactionRepository.update(id, input);
      await pubsub.publish(
        TRANSACTION_UPDATED,
        {
          transactionUpdated: transaction,
        },
      );
      return transaction;
    },
    deleteTransaction: async (_, { id }) => {
      const transaction: any = await TransactionRepository.delete(id);
      await pubsub.publish(
        TRANSACTION_DELETED,
        {
          transactionDeleted: transaction,
        },
      );
      return transaction;
    },
  },

  Subscription: {
    transactionCreated: {
      subscribe: () => pubsub.asyncIterator([TRANSACTION_CREATED]),
    },
    transactionUpdated: {
      subscribe: () => pubsub.asyncIterator([TRANSACTION_UPDATED]),
    },
    transactionDeleted: {
      subscribe: () => pubsub.asyncIterator([TRANSACTION_DELETED]),
    },
  },
};
