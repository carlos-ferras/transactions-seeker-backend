export const transactionTypeDefs = `
  type GeoInfo {
    latitude: Float!
    longitude: Float!
  }
  
  type ConnectionInfo {
    type: String!
    confidence: Float!
  }
  
  type Transaction {
    id: ID!
    name: String!
    email: String!
    phone: String!
    age: Int!
    geoInfo: GeoInfo!
    connectionInfo: ConnectionInfo
    childrens: [Transaction!]
  }
  
  input GeoInfoInput {
    latitude: Float!
    longitude: Float!
  }
  
  input ConnectionInfoInput {
    type: String!
    confidence: Float!
  }
  
  input TransactionChildInput {
    id: String!
    name: String!
    email: String!
    phone: String!
    age: Int!
    geoInfo: GeoInfoInput!
    connectionInfo: ConnectionInfoInput!
    childrens: [TransactionChildInput!]
  }
  
  input TransactionInput {
    id: String!
    name: String!
    email: String!
    phone: String!
    age: Int!
    geoInfo: GeoInfoInput!
    childrens: [TransactionChildInput!]
  }
  
  input TransactionUpdateInput {
    name: String
    email: String
    phone: String
    age: Int
    geoInfo: GeoInfoInput
    connectionInfo: ConnectionInfoInput
  }
  
  extend type Query {
    transactions: [Transaction]
    transaction(id: String!): Transaction
  }
  
  extend type Mutation {
    addTransaction(input: TransactionInput!): Transaction
    updateTransaction(id: String!, input: TransactionUpdateInput!): Transaction
    deleteTransaction(id: String!): Transaction
  }
  
  extend type Subscription {
    transactionCreated: Transaction
    transactionUpdated: Transaction
    transactionDeleted: Transaction
  }
`;
