const compilerOptions = require("./tsconfig.json").compilerOptions;

module.exports = function (wallaby) {
  return {
    files: [
      "src/**/*.ts?(x)",
      { pattern: 'package.json', load: false },
      { pattern: 'tsconfig.json', load: false },
      { pattern: 'babel.conf.js', load: false },
      { pattern: 'src/**/*.spec.ts', ignore: true }
    ],

    tests: [
      "src/**/*.spec.ts"
    ],

    env: {
      type: 'node',
      runner: 'node'
    },

    compilers: {
      "src/**/*.ts?(x)": wallaby.compilers.typeScript(compilerOptions)
    },

    testFramework: "mocha",

    ignoreFileLoadingDependencyTracking: true,

    debug: true
  };
};
