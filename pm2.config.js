module.exports = {
  apps: [
    {
      name: 'transactions-seeker',
      script: './dist/main.js',
      watch: false,
      restart_delay: 1000,
      out_file: './logs/out.log',
      error_file: './logs/error.log',
      merge_logs: true,
      exec_mode: "cluster_mode",
      instances: 2,
      log_date_format: "DD-MM-YYYY HH:mm Z"
    },
  ],
};
